package main.repository.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSubject is a Querydsl query type for Subject
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSubject extends EntityPathBase<Subject> {

    private static final long serialVersionUID = 282729776L;

    public static final QSubject subject = new QSubject("subject");

    public final NumberPath<Integer> hour = createNumber("hour", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath lectorName = createString("lectorName");

    public final StringPath name = createString("name");

    public final StringPath roomNumber = createString("roomNumber");

    public final ListPath<Student, QStudent> students = this.<Student, QStudent>createList("students", Student.class, QStudent.class, PathInits.DIRECT2);

    public final EnumPath<WeekDay> weekDay = createEnum("weekDay", WeekDay.class);

    public QSubject(String variable) {
        super(Subject.class, forVariable(variable));
    }

    public QSubject(Path<? extends Subject> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSubject(PathMetadata metadata) {
        super(Subject.class, metadata);
    }

}


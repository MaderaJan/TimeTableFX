package main.repository.student;

import main.repository.entity.Student;

import java.util.List;

public interface StudentRepository {

    Student findStudentById(long id);

    List<Student> findStudentsByIds(List<Long> ids);

    List<Student> selectAllStudents();

    void saveStudent(Student student);

    void saveStudents(List<Student> list);
}

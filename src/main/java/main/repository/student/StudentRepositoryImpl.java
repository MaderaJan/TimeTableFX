package main.repository.student;

import com.querydsl.jpa.impl.JPAQuery;
import main.repository.entity.QStudent;
import main.repository.entity.Student;
import main.services.ServiceLocator;

import javax.persistence.EntityManager;
import java.util.List;

public class StudentRepositoryImpl implements StudentRepository {

    public Student findStudentById(long id) {
        EntityManager entityManager = ServiceLocator.createEntityManager();

        JPAQuery<Student> query = new JPAQuery<Student>(entityManager);
        QStudent qStudent = QStudent.student;

        return query.select(qStudent)
                .from(qStudent)
                .where(qStudent.id.eq(id))
                .fetchOne();
    }

    @Override
    public List<Student> findStudentsByIds(List<Long> ids) {
        EntityManager entityManager = ServiceLocator.createEntityManager();

        JPAQuery<Student> query = new JPAQuery<Student>(entityManager);
        QStudent qStudent = QStudent.student;

        return query.select(qStudent)
                .from(qStudent)
                .where(qStudent.id.in(ids))
                .fetch();
    }

    public List<Student> selectAllStudents() {
        EntityManager entityManager = ServiceLocator.createEntityManager();

        JPAQuery<Student> query = new JPAQuery<Student>(entityManager);
        QStudent qStudent = QStudent.student;

        return query.select(qStudent)
                .from(qStudent)
                .fetch();
    }

    public void saveStudent(Student student) {
        EntityManager entityManager = ServiceLocator.createEntityManager();
        entityManager.getTransaction().begin();

        if(student.getId() == null){
            entityManager.persist(student);
        }else{
            entityManager.merge(student);
        }

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void saveStudents(List<Student> list) {
        EntityManager entityManager = ServiceLocator.createEntityManager();
        entityManager.getTransaction().begin();

        for (Student s : list){

            if(s.getId() == null){
                entityManager.persist(s);
            }else{
                entityManager.merge(s);
            }
        }

        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
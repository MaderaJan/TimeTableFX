package main.repository.subject;

import com.querydsl.jpa.impl.JPAQuery;

import main.repository.entity.QSubject;
import main.repository.entity.Subject;
import main.repository.entity.WeekDay;
import main.services.ServiceLocator;

import javax.persistence.EntityManager;
import java.util.List;

public class SubjectRepositoryImpl implements SubjectRepository {

    public Subject findSubjectById(Long id) {
        EntityManager entityManager = ServiceLocator.createEntityManager();

        JPAQuery<Subject> query = new JPAQuery<Subject>(entityManager);
        QSubject qSubject = QSubject.subject;

        return query.select(qSubject)
                .from(qSubject)
                .where(qSubject.id.eq(id))
                .fetchOne();
    }

    public List<Subject> findNotRegisteredSubjects(List<Subject> registeredSubjects) {
        EntityManager entityManager = ServiceLocator.createEntityManager();

        JPAQuery<Subject> query = new JPAQuery<Subject>(entityManager);
        QSubject qSubject = QSubject.subject;

        return query.select(qSubject)
                .from(qSubject)
                .where(qSubject.notIn(registeredSubjects))
                .fetch();
    }

    public List<Subject> selectAllSubjects() {
        EntityManager entityManager = ServiceLocator.createEntityManager();

        JPAQuery<Subject> query = new JPAQuery<Subject>(entityManager);
        QSubject qSubject = QSubject.subject;

        return query.select(qSubject)
                .from(qSubject)
                .fetch();
    }

    @Override
    public Subject findSubjectByDayHourRoomNumber(WeekDay day, int hour, String roomNumber) {
        EntityManager entityManager = ServiceLocator.createEntityManager();

        JPAQuery<Subject> query = new JPAQuery<Subject>(entityManager);
        QSubject qSubject = QSubject.subject;

        return query.select(qSubject)
                .from(qSubject)
                .where(qSubject.weekDay.eq(day)
                        .and(qSubject.hour.eq(hour))
                        .and(qSubject.roomNumber.eq(roomNumber)))
                .fetchOne();
    }

    public void saveSubject(Subject subject) {
        EntityManager entityManager = ServiceLocator.createEntityManager();
        entityManager.getTransaction().begin();

        if(subject.getId() == null){
            entityManager.persist(subject);
        }else{
            entityManager.merge(subject);
        }

        entityManager.getTransaction().commit();
        entityManager.close();
    }
}

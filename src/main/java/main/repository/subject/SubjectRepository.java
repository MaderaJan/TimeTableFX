package main.repository.subject;

import main.repository.entity.Subject;
import main.repository.entity.WeekDay;

import java.util.List;

public interface SubjectRepository {

    Subject findSubjectById(Long id);

    List<Subject> findNotRegisteredSubjects(List<Subject> registeredSubjects);

    List<Subject> selectAllSubjects();

    Subject findSubjectByDayHourRoomNumber(WeekDay day, int hour, String roomNumber);

    void saveSubject(Subject subject);
}

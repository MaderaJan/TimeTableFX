package main.repository.entity;

public enum WeekDay {
    MONDAY("Pondělí", 0),
    TUDAYS("Úterý", 1),
    WENDEDAYS("Středa", 2),
    THRUDAYS("Čtvrtek", 3),
    FRIDAY("Pátek", 4);

    private final String name;
    private final int value;

    WeekDay(String name, int value){
        this.name = name;
        this.value = value;
    }

    public int getValue(){
        return value;
    }

    @Override
    public String toString() {
        return name;
    }
}

package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.repository.entity.Student;
import main.repository.entity.Subject;
import main.services.ServiceLocator;
import main.services.student.StudentService;

import java.util.Collections;
import java.util.List;

import static main.utils.Constants.TITLE;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/ui/main_window.fxml"));
        primaryStage.setTitle(TITLE);
        primaryStage.setScene(new Scene(root, 1024, 653));
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        ServiceLocator.shutDown();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

package main.services.subject;

import main.repository.entity.Subject;
import main.repository.entity.WeekDay;

import java.util.List;

public interface SubjectService {

    Subject getSubjectById(Long id);

    List<Subject> getNotRegisteredSubjectsByStudent(List<Subject> registeredSubjects);

    List<Subject> getAllSubjects();

    Subject canBeSubjectCreated(WeekDay day, int hour, String roomNumber);

    void saveSubject(Subject subject);

    void prepareTestData();
}

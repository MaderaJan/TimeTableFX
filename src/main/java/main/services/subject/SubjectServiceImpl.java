package main.services.subject;

import main.repository.entity.Subject;
import main.repository.entity.WeekDay;
import main.services.ServiceLocator;
import main.utils.GeneratorUtil;

import java.util.List;

public class SubjectServiceImpl implements SubjectService {

    public Subject getSubjectById(Long id) {
        return ServiceLocator.getSubjectRepository().findSubjectById(id);
    }

    public List<Subject> getNotRegisteredSubjectsByStudent(List<Subject> registeredSubjects) {
        return ServiceLocator.getSubjectRepository().findNotRegisteredSubjects(registeredSubjects);
    }

    public List<Subject> getAllSubjects() {
        return ServiceLocator.getSubjectRepository().selectAllSubjects();
    }

    @Override
    public Subject canBeSubjectCreated(WeekDay day, int hour, String roomNumber) {
        return ServiceLocator.getSubjectRepository()
                .findSubjectByDayHourRoomNumber(day, hour, roomNumber);
    }

    public void saveSubject(Subject subject) {
        ServiceLocator.getSubjectRepository().saveSubject(subject);
    }

    @Override
    public void prepareTestData() {
        for (int i = 0; i < 20; i++){
            Subject subject = new Subject();
            subject.setName("Subject" + i);
            subject.setLectorName("Lector" + i);
            subject.setRoomNumber("Room" + i);
            subject.setHour(GeneratorUtil.generateNumber(8, 16));
            subject.setWeekDay(GeneratorUtil.generateWeekDay());
            saveSubject(subject);
        }
    }
}

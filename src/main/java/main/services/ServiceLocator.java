package main.services;

import main.repository.student.StudentRepository;
import main.repository.student.StudentRepositoryImpl;
import main.repository.subject.SubjectRepository;
import main.repository.subject.SubjectRepositoryImpl;
import main.services.student.StudentService;
import main.services.student.StudentServiceImpl;
import main.services.subject.SubjectService;
import main.services.subject.SubjectServiceImpl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ServiceLocator {

    private static final String PERSISTENCE_UNIT_NAME = "timetable";

    private static EntityManagerFactory entityManagerFactory;

    private static StudentRepository studentRepository;
    private static StudentService studentService;

    private static SubjectRepository subjectRepository;
    private static SubjectService subjectService;

    public static EntityManager createEntityManager(){
        return getEntityManagerFactory().createEntityManager();
    }

    private static EntityManagerFactory getEntityManagerFactory(){
        if(entityManagerFactory == null){
            entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        }
        return entityManagerFactory;
    }

    public static StudentRepository getStudentRepository(){
        if(studentRepository == null){
            studentRepository = new StudentRepositoryImpl();
        }

        return studentRepository;
    }

    public static StudentService getStudentService(){
        if(studentService == null){
            studentService = new StudentServiceImpl();
        }

        return studentService;
    }

    public static SubjectRepository getSubjectRepository(){
        if(subjectRepository == null){
            subjectRepository = new SubjectRepositoryImpl();
        }

        return subjectRepository;
    }

    public static SubjectService getSubjectService(){
        if(subjectService == null){
            subjectService = new SubjectServiceImpl();
        }

        return subjectService;
    }

    public static void shutDown(){
        //createEntityManager().close();
        entityManagerFactory.close();
    }
}

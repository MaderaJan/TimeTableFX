package main.services.student;

import main.repository.entity.Student;
import main.repository.entity.Subject;

import java.util.List;

public interface StudentService {

    Student getStudentById(long id);

    List<Student> getAllStudents();

    List<Subject> getSubjectsOrderedByHours(long id);

    List<Student> getStudentsWithSubjectsOrderedByHours(List<Long> ids);

    void saveStudent(Student student);

    void prepareTestData();
}

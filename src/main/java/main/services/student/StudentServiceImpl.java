package main.services.student;

import main.repository.entity.Student;
import main.repository.entity.Subject;
import main.services.ServiceLocator;

import java.util.*;

public class StudentServiceImpl implements StudentService {

    public Student getStudentById(long id) {
        return ServiceLocator.getStudentRepository().findStudentById(id);
    }

    public List<Student> getAllStudents() {
        return ServiceLocator.getStudentRepository().selectAllStudents();
    }

    public List<Subject> getSubjectsOrderedByHours(long id) {
        Student student = getStudentById(id);

        List<Subject> subjects = student.getSubjects();
        Collections.sort(subjects, Comparator.comparing(Subject::getHour));
        return subjects;
    }

    @Override
    public List<Student> getStudentsWithSubjectsOrderedByHours(List<Long> ids) {
        List<Student> students = ServiceLocator.getStudentRepository().findStudentsByIds(ids);

        for (Student student : students) {
            Collections.sort(student.getSubjects(), Comparator.comparing(Subject::getHour));
        }

        return students;
    }

    public void saveStudent(Student student) {
        ServiceLocator.getStudentRepository().saveStudent(student);
    }

    public void prepareTestData() {
        for (int i = 0; i < 10; i++){
            Student student = new Student();
            student.setUserName("username" + i);
            student.setFirstName("first_name" + i);
            student.setLastName("last_name" + i);
            student.setEmail("email" + i + "@mail.com");
            saveStudent(student);
        }
    }
}

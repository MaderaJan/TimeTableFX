package main.utils;

import main.repository.entity.WeekDay;

import java.util.Random;

public class GeneratorUtil {

    public static int generateNumber(int min, int max){
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    public static WeekDay generateWeekDay(){
        switch (generateNumber(0, 4)){
            case 0:
                return WeekDay.MONDAY;
            case 1:
                return WeekDay.TUDAYS;
            case 2:
                return WeekDay.WENDEDAYS;
            case 3:
                return WeekDay.THRUDAYS;
            default:
                return WeekDay.FRIDAY;
        }
    }
}

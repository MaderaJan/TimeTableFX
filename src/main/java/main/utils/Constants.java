package main.utils;

public class Constants {

    public static final int FIRST_HOUR = 7;
    public static final int LAST_HOUR = 19;
    public static final int NUMBER_OF_SCHOOL_DAYS = 5;

    public static final String TITLE = "Rozvrh hodin";

    public static final String BEGIN_HOUR = " : 00\n\n";
    public static final String END_HOUR = " : 45";

    public static final String FREE_DAY = "VOLNO";
    public static final String BUSY_DAY = "XXXXX";
    public static final String TIME_TOGETHER = "Společný čas";
    public static final String THIS_DATE_IS_ALREADY_REGISTER = "V tento termín je zapsán předmět";
    public static final String ALL_FIELDS_FILLED = "Všechny pole musí být vyplněna.";

    public static final String ERROR = "Chyba";
    public static final String EDIT = "UPRAVIT";
    public static final String CANNOT_REGISTER = "Nelze zaregistrovat";
    public static final String CANNOT_CREATE = "Nelze vytvořit";

    public static final String CREATE_STUDENT = "Vytvoření studenta.";
    public static final String STUDENT_EDITING = "Editace studenta";
    public static final String CHOOSE_STUDENT = "Prosím vyberte studenta.";
    public static final String CHOOSE_AT_LEAST_TWO_STUDENTS = "Musíte vybrát nejméně 2 studenty.";

    public static final String DEAKTIVATION = "Deaktivace.";
    public static final String DEAKTIVATION_STUDENT = "Deaktivace studenta.";
    public static final String DEAKTIVATION_STUDENT_QUESTION = "Opravdu chcete studenta deaktivovat? (Jeho předměty budou smazány)";

    public static final String CREATE_SUBJECT = "Vytvoření předmětu.";
    public static final String REGISTER_SUBJECT = "Zapsat předmět";
    public static final String EDIT_SUBJECT = "Upravení předmětu.";
    public static final String CHOOSE_SUBJECT = "Prosím vyberte předmět.";

    public static final String UNREGISTER = "Odhlášení";
    public static final String UNREGISTER_SUBJECT = "Odhlášení předmětu";
    public static final String UNREGISTER_SUBJECT_QUESTION = "Opravdu chcete předmět odhlásit?";

}

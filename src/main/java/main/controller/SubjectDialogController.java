package main.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import main.controller.dialog.Dialogs;
import main.repository.entity.Subject;
import main.repository.entity.WeekDay;
import main.services.ServiceLocator;

import java.net.URL;
import java.util.ResourceBundle;

import static main.repository.entity.WeekDay.*;
import static main.utils.Constants.*;

public class SubjectDialogController implements Initializable{

    @FXML
    private Button createSubjectButton;

    @FXML
    private TextField nameTextField;

    @FXML
    private TextField lectorTextField;

    @FXML
    private TextField roomTextField;

    @FXML
    private Spinner<Integer> hourSpinner;

    @FXML
    private ComboBox<WeekDay> weekDaysComboBox;

    private Stage dialogStage;
    private Subject subject;

    public SubjectDialogController(){
        subject = new Subject();
    }

    public void initialize(URL location, ResourceBundle resources) {
        hourSpinner.setValueFactory(
                new SpinnerValueFactory.IntegerSpinnerValueFactory(7, 19, 10));

        ObservableList<WeekDay> dayOfWeek = FXCollections.observableArrayList(
                MONDAY, TUDAYS, WENDEDAYS, THRUDAYS, FRIDAY
        );
        weekDaysComboBox.setItems(dayOfWeek);
        weekDaysComboBox.getSelectionModel().select(0);
    }

    public void cancelDialog(ActionEvent actionEvent) {
        subject = null;
        dialogStage.close();
    }

    public void createSubject(ActionEvent actionEvent) {
        if(allFieldsFilled()){
            subject.setName(nameTextField.getText());
            subject.setLectorName(lectorTextField.getText());
            subject.setRoomNumber(roomTextField.getText());
            subject.setHour(hourSpinner.getValue());
            subject.setWeekDay(weekDaysComboBox.getValue());

            Subject tmp =
                    ServiceLocator
                            .getSubjectService()
                            .canBeSubjectCreated(
                                    subject.getWeekDay(), subject.getHour(), subject.getRoomNumber());

            if(tmp == null){
                dialogStage.close();
            }else{
                Dialogs.showAlertDialog(CANNOT_CREATE,
                        "Přědmět " + tmp.getName() + " je pro tento termín zaregisrován");
            }
        }else{
            Dialogs.showAlertDialog(ERROR, ALL_FIELDS_FILLED);
        }
    }

    private boolean allFieldsFilled() {
        return !(nameTextField.getText().isEmpty()
                || lectorTextField.getText().isEmpty()
                || roomTextField.getText().isEmpty());
    }

    protected void setSubjectFields(Subject subject){
        this.subject = subject;

        nameTextField.setText(subject.getName());
        lectorTextField.setText(subject.getLectorName());
        roomTextField.setText(subject.getRoomNumber());
        weekDaysComboBox.getSelectionModel().select(subject.getWeekDay());

        hourSpinner.setValueFactory(
                new SpinnerValueFactory.IntegerSpinnerValueFactory(FIRST_HOUR, LAST_HOUR, subject.getHour()));

        createSubjectButton.setText(EDIT);
    }

    protected void setDialogStage(Stage dialogStage){
        this.dialogStage = dialogStage;
    }

    protected Stage getDialoStage(){
        return dialogStage;
    }

    protected Subject getResult() {
        return subject;
    }
}

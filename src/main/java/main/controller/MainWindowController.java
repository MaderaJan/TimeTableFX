package main.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.controller.dialog.Dialogs;
import main.repository.entity.Student;
import main.repository.entity.Subject;
import main.services.ServiceLocator;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import static main.utils.Constants.*;

public class MainWindowController implements Initializable{

    //STUDENT PANE
    @FXML
    private TableView<Student> studentTableView;

    @FXML
    private TableColumn<Student, String> firstNameColumn;

    @FXML
    private TableColumn<Student, String> lastNameColumn;

    @FXML
    private TableView<Subject> studentSubjectsTableView;

    @FXML
    private TableColumn<Subject, String> studentSubjectNameColumn;

    @FXML
    private TableColumn<Subject, String> studentSubjectLectorColumn;

    @FXML
    private TableColumn<Subject, String> studentSubjectDayColumn;

    @FXML
    private Label usernameLabel;

    @FXML
    private Label firstNameLabel;

    @FXML
    private Label lastNameLabel;

    @FXML
    private Label emailLabel;

    @FXML
    private TabPane timeTableTabPane;

    //SUBJECT PANE
    @FXML
    private TableView<Subject> subjectsTableView;

    @FXML
    private TableColumn<Subject, String> nameColumn;

    @FXML
    private TableColumn<Subject, String> lectorColumn;

    @FXML
    private TableColumn<Subject, String> roomColumn;

    @FXML
    private Label subjectNameLabel;

    @FXML
    private Label subjectLectorLabel;

    @FXML
    private Label subjectRoomLabel;

    @FXML
    private Label hourLabel;

    @FXML
    private Label dayOfWeekLabel;

    @FXML
    private TableView<Student> subjectStudentTableView;

    @FXML
    private TableColumn<Student, String> subjectStudentFirstNameColumn;

    @FXML
    private TableColumn<Student, String> subjectStudentLastNameColumn;

    @FXML
    private TableColumn<Student, String> subjectStudentusernameColumn;

    public void initialize(URL location, ResourceBundle resources) {
        studentTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        studentTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Student>() {
            public void changed(ObservableValue<? extends Student> observable, Student oldValue, Student newValue) {
                if(newValue != null){
                    loadStudentDetails(newValue.getId());
                    createStudentTimeTable();
                }
            }
        });
        loadStudents();

        subjectsTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Subject>() {
            public void changed(ObservableValue<? extends Subject> observable, Subject oldValue, Subject newValue) {
                if(newValue != null){
                    loadSubjectDetail(newValue.getId());
                }
            }
        });
        loadSubjects();
    }

    public void showCreateStudentDialog(ActionEvent actionEvent) throws IOException {
        StudentDialogController controller = getStudentDialogController(CREATE_STUDENT);

        controller.getDialogStage().showAndWait();

        if(controller.getResult() != null){
            ServiceLocator.getStudentService().saveStudent(controller.getResult());
            loadStudents();
        }
    }

    public void editStudentDialog(ActionEvent actionEvent) throws IOException {
        Student student = studentTableView.getSelectionModel().getSelectedItem();
        if(student != null){
            StudentDialogController controller = getStudentDialogController(STUDENT_EDITING);
            controller.setStudent(student);

            controller.getDialogStage().showAndWait();

            if(controller.getResult() != null){
                ServiceLocator.getStudentService().saveStudent(controller.getResult());
                loadStudents();
            }
        }else{
            Dialogs.showAlertDialog(ERROR, CHOOSE_STUDENT);
        }
    }

    public void showRegisterDialog(ActionEvent actionEvent) throws IOException {
        Student student = studentTableView.getSelectionModel().getSelectedItem();

        if(student != null && student.getSubjects() != null)  {
            List<Subject> unRegisteredSubjects =
                    ServiceLocator.getSubjectService().getNotRegisteredSubjectsByStudent(student.getSubjects());

            ChooseSubjectDialogController controller = getChooseSubjectDialogController();
            controller.setSubjects(unRegisteredSubjects);
            controller.setRegisteredSubjects(student.getSubjects());

            controller.getDialogStage().showAndWait();

            if (controller.getResult() != null) {
                student.addSubject(controller.getResult());
                ServiceLocator.getStudentRepository().saveStudent(student);
                loadStudentSubjects(student);
                createStudentTimeTable();
            }
        }else{
            Dialogs.showAlertDialog(ERROR, CHOOSE_STUDENT);
        }
    }

    public void showCreateSubjectDialog(ActionEvent actionEvent) throws IOException {
        SubjectDialogController controller = getSubjectDialogController(CREATE_SUBJECT);

        controller.getDialoStage().showAndWait();

        if(controller.getResult() != null){
            ServiceLocator.getSubjectService().saveSubject(controller.getResult());
            loadSubjects();
        }
    }


    public void showEditSubjectDialog(ActionEvent actionEvent) throws IOException {
        Subject subject = subjectsTableView.getSelectionModel().getSelectedItem();

        if(subject != null){
            SubjectDialogController controller = getSubjectDialogController(EDIT_SUBJECT);
            controller.setSubjectFields(subject);

            controller.getDialoStage().showAndWait();

            if(controller.getResult() != null){
                ServiceLocator.getSubjectService().saveSubject(controller.getResult());
                loadSubjects();
            }
        }else{
            Dialogs.showAlertDialog(ERROR, CHOOSE_STUDENT);
        }
    }

    public void unregisterSubject(ActionEvent actionEvent) {
        Student student = studentTableView.getSelectionModel().getSelectedItem();
        Subject unRegSubject = studentSubjectsTableView.getSelectionModel().getSelectedItem();

        if(student == null){
            Dialogs.showAlertDialog(ERROR, CHOOSE_STUDENT);
        }else if(unRegSubject == null){
            Dialogs.showAlertDialog(ERROR, CHOOSE_SUBJECT);
        }else {

            if(Dialogs.showYesNoDialog(UNREGISTER, UNREGISTER_SUBJECT, UNREGISTER_SUBJECT_QUESTION)){
                student.unRegisterSubject(unRegSubject);

                ServiceLocator.getStudentRepository().saveStudent(student);
                loadStudentSubjects(student);
            }
        }
    }

    public void deactivateStudent(ActionEvent actionEvent) {
        Student student = studentTableView.getSelectionModel().getSelectedItem();
        if(student != null){
            if(Dialogs.showYesNoDialog(DEAKTIVATION, DEAKTIVATION_STUDENT,DEAKTIVATION_STUDENT_QUESTION)){
                student.getSubjects().clear();
                ServiceLocator.getStudentService().saveStudent(student);
                loadStudentDetails(student.getId());
                loadStudentSubjects(student);
                createStudentTimeTable();
            }
        }else{
            Dialogs.showAlertDialog(ERROR, CHOOSE_STUDENT);
        }
    }

    public void freeTimeTogether(ActionEvent actionEvent) {
        List<Student> students = studentTableView.getSelectionModel().getSelectedItems();
        if(students.isEmpty() || students.size() == 1) {
            Dialogs.showAlertDialog(ERROR, CHOOSE_AT_LEAST_TWO_STUDENTS);
        }else{

            String[][] days = getFreeDaysMatrix(students);
            createFreeTimeTable(days);
        }
    }

    private String[][] getFreeDaysMatrix(List<Student> students) {
        String[][] days = new String[NUMBER_OF_SCHOOL_DAYS][FIRST_HOUR - LAST_HOUR + 2];
        for (int day = 0; day < days.length; day++){
            for (int hour = 0; hour < days[0].length; hour++){
                boolean freeTime = true;
                for (Student student : students) {
                    for (Subject subject : student.getSubjects()) {
                        if(haveHour(day, hour, subject)){
                            freeTime = false;
                            break;
                        }
                    }
                    if(!freeTime) break;
                }

                days[day][hour] = freeTime ? FREE_DAY : BUSY_DAY;
            }
        }
        return days;
    }

    private boolean haveHour(int day, int hour, Subject subject) {
        return subject.getWeekDay().getValue() == day && subject.getHour() == hour + FIRST_HOUR;
    }

    private void createFreeTimeTable(String[][] days){
        timeTableTabPane.getTabs().clear();

        Tab newTab = new Tab(TIME_TOGETHER);

        GridPane freeTimeGridPane = createFreeTimeGridPane(days);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(freeTimeGridPane);

        newTab.setContent(scrollPane);

        timeTableTabPane.getTabs().add(newTab);
    }

    private GridPane createFreeTimeGridPane(String[][] days){
        GridPane gridPane = new GridPane();
        gridPane.setGridLinesVisible(true);
        for (int i = 0; i < 6; i++){
            gridPane.getColumnConstraints().add(new ColumnConstraints(100));
        }

        int hourNumber = FIRST_HOUR;
        for (int day = 0; day < days.length + 1; day++){
            for (int hour = 0; hour < days[0].length; hour++){
                if(day == 0){
                    String text = hourNumber + BEGIN_HOUR + hourNumber + END_HOUR;
                    addLabelToPane(gridPane, day, hour, text);
                    hourNumber++;
                }else{
                    addLabelToPane(gridPane, day, hour, days[day-1][hour]);
                }
            }
        }

        return gridPane;
    }

    private SubjectDialogController getSubjectDialogController(String title) throws IOException {
        FXMLLoader loader = getFxmlLoader("/ui/subject_dialog.fxml");
        AnchorPane page = loader.load();

        Stage diaStage = setUpStage(title, page);

        SubjectDialogController controller = loader.getController();
        controller.setDialogStage(diaStage);

        return controller;
    }

    private StudentDialogController getStudentDialogController(String title) throws IOException {
        FXMLLoader loader = getFxmlLoader("/ui/student_dialog.fxml");
        AnchorPane page = loader.load();

        Stage dialogStage = setUpStage(title, page);

        StudentDialogController controller = loader.getController();
        controller.setDialogStage(dialogStage);

        return controller;
    }

    private ChooseSubjectDialogController getChooseSubjectDialogController() throws IOException {
        FXMLLoader loader = getFxmlLoader("/ui/choose_subject_dialog.fxml");
        AnchorPane page = loader.load();

        Stage dialogStage = setUpStage(REGISTER_SUBJECT, page);

        ChooseSubjectDialogController controller = loader.getController();
        controller.setDialogStage(dialogStage);

        return controller;
    }

    private Stage setUpStage(String title, AnchorPane page){
        Stage dialogStage = new Stage();
        dialogStage.setTitle(title);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        Scene scene = new Scene(page);
        dialogStage.setScene(scene);

        return dialogStage;
    }

    private FXMLLoader getFxmlLoader(String path){
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(path));
        return loader;
    }

    private void loadStudents(){
        ObservableList<Student> students =
                FXCollections.observableArrayList(ServiceLocator.getStudentService().getAllStudents());

        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Student, String>("lastName"));

        studentTableView.setItems(students);
    }

    private void loadStudentDetails(long studentId){
        Student student = ServiceLocator.getStudentService().getStudentById(studentId);

        usernameLabel.setText(student.getUserName());
        firstNameLabel.setText(student.getFirstName());
        lastNameLabel.setText(student.getLastName());
        emailLabel.setText(student.getEmail());

        loadStudentSubjects(student);
    }

    private void loadStudentSubjects(Student student) {
        ObservableList<Subject> subjects =
                FXCollections.observableArrayList(student.getSubjects());

        studentSubjectNameColumn.setCellValueFactory(new PropertyValueFactory<Subject, String>("name"));
        studentSubjectLectorColumn.setCellValueFactory(new PropertyValueFactory<Subject, String>("lectorName"));
        studentSubjectDayColumn.setCellValueFactory(new PropertyValueFactory<Subject, String>("weekDay"));

        studentSubjectsTableView.setItems(subjects);
    }

    private void loadSubjects(){
        ObservableList<Subject> subjects =
                FXCollections.observableArrayList(ServiceLocator.getSubjectService().getAllSubjects());

        nameColumn.setCellValueFactory(new PropertyValueFactory<Subject, String>("name"));
        lectorColumn.setCellValueFactory(new PropertyValueFactory<Subject, String>("lectorName"));
        roomColumn.setCellValueFactory(new PropertyValueFactory<Subject, String>("roomNumber"));

        subjectsTableView.setItems(subjects);
    }

    private void loadSubjectDetail(Long subjectId) {
        Subject subject = ServiceLocator.getSubjectService().getSubjectById(subjectId);

        subjectNameLabel.setText(subject.getName());
        subjectLectorLabel.setText(subject.getLectorName());
        subjectRoomLabel.setText(subject.getRoomNumber());
        hourLabel.setText(subject.getHour() + " : 00 - " + subject.getHour() + " : 45");
        dayOfWeekLabel.setText(subject.getWeekDay().toString());

        loadSubjectStudents(subject);
    }

    private void loadSubjectStudents(Subject subject) {
        ObservableList<Student> students = FXCollections.observableArrayList(subject.getStudents());

        subjectStudentFirstNameColumn
                .setCellValueFactory(new PropertyValueFactory<Student, String >("firstName"));
        subjectStudentLastNameColumn
                .setCellValueFactory(new PropertyValueFactory<Student, String >("lastName"));
        subjectStudentusernameColumn
                .setCellValueFactory(new PropertyValueFactory<Student, String >("username"));

        subjectStudentTableView.setItems(students);
    }

    private void createStudentTimeTable(){
        timeTableTabPane.getTabs().clear();

        List<Student> students = studentTableView.getSelectionModel().getSelectedItems();
        for (Student student: students) {
            createTabWithTimeTable(student);
        }
    }

    private void createTabWithTimeTable(Student student){
        Tab newTab = new Tab(student.getLastName());

        GridPane timeTableGridPane = createGridPane(student.getId());
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(timeTableGridPane);

        newTab.setContent(scrollPane);

        timeTableTabPane.getTabs().add(newTab);
    }

    private GridPane createGridPane(long id){
        GridPane gridPane = new GridPane();
        gridPane.setGridLinesVisible(true);

        for (int i = 0; i < 6; i++){
            gridPane.getColumnConstraints().add(new ColumnConstraints(100));
        }

        List<Subject> orderSubjects = ServiceLocator.getStudentService().getSubjectsOrderedByHours(id);
        createTimeTableForStudent(orderSubjects, gridPane);

        return gridPane;
    }

    private void createTimeTableForStudent(List<Subject> subjects, GridPane gridPane){
        final int rowBegin = FIRST_HOUR;
        int hourNumber = -1;

        for (Subject s : subjects){
            if(hourNumber != s.getHour()){
                hourNumber = s.getHour();
                String text = hourNumber + BEGIN_HOUR + hourNumber + END_HOUR;
                addLabelToPane(gridPane, 0, hourNumber - rowBegin, text);
            }

            int column = s.getWeekDay().getValue() + 1;
            int row = s.getHour() - rowBegin;
            String text = s.getName()  + "\n" + s.getLectorName() + "\n" + s.getRoomNumber();
            addLabelToPane(gridPane, column, row, text);
        }
    }

    private void addLabelToPane(GridPane gridPane, int columnIndex, int rowIndex, String text){
        Label label = new Label(text);
        label.setMinHeight(100);
        label.setPrefHeight(100);
        label.setMaxHeight(100);
        label.setTextAlignment(TextAlignment.CENTER);
        label.setContentDisplay(ContentDisplay.CENTER);
        label.setPadding(new Insets(5));
        label.setWrapText(true);

        gridPane.add(label, columnIndex, rowIndex);
    }

    public void prepareTestData(ActionEvent actionEvent) {
        ServiceLocator.getStudentService().prepareTestData();
        ServiceLocator.getSubjectService().prepareTestData();
        registerSubjects();
    }

    private void registerSubjects(){
        List<Subject> subjects = ServiceLocator.getSubjectService().getAllSubjects();
        List<Student> students = ServiceLocator.getStudentService().getAllStudents();

        for (Student student : students) {
            for (int i = 0; i < 5 ; i++){
                student.addSubject(subjects.get(i));
            }
            Collections.shuffle(subjects);
        }

        ServiceLocator.getStudentRepository().saveStudents(students);
        loadSubjects();
        loadStudents();
    }
}

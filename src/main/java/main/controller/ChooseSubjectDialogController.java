package main.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import main.controller.dialog.Dialogs;
import main.repository.entity.Subject;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import static main.utils.Constants.CANNOT_REGISTER;
import static main.utils.Constants.THIS_DATE_IS_ALREADY_REGISTER;

public class ChooseSubjectDialogController implements Initializable{

    @FXML
    private ComboBox<Subject> subjectsComboBox;

    @FXML
    private Button registerSubjectButton;

    @FXML
    private Label lectorLabel;

    @FXML
    private Label roomLabel;

    @FXML
    private Label hourLabel;

    @FXML
    private Label dayLabel;

    private Stage dialogStage;
    private Subject subject;
    private List<Subject> registeredSubjects;

    public void initialize(URL location, ResourceBundle resources) {
        subjectsComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Subject>() {
            public void changed(ObservableValue<? extends Subject> observable, Subject oldValue, Subject newValue) {
                setSubjectLabels(newValue);
            }
        });
    }

    public void registerSubject(ActionEvent actionEvent) {
        subject = subjectsComboBox.getSelectionModel().getSelectedItem();

        boolean canBeRegistered = true;
        String alreadyRegisteredSubject = "";
        for (Subject regSubject : registeredSubjects){
            if(subject.getHour() == regSubject.getHour() &&
                    subject.getWeekDay() == regSubject.getWeekDay()){
                canBeRegistered = false;
                alreadyRegisteredSubject = regSubject.getName();
                break;
            }
        }

        if(canBeRegistered){
            dialogStage.close();
        }else{
            Dialogs.showAlertDialog(CANNOT_REGISTER,
                    THIS_DATE_IS_ALREADY_REGISTER + " " + alreadyRegisteredSubject);
        }
    }

    public void cancelDialog(ActionEvent actionEvent) {
        subject = null;
        dialogStage.close();
    }

    private void setSubjectLabels(Subject subject) {
        lectorLabel.setText(subject.getLectorName());
        roomLabel.setText(subject.getRoomNumber());
        dayLabel.setText(subject.getWeekDay().toString());
        hourLabel.setText(subject.getHour() + " : 00 - " + subject.getHour() + " : 45");
    }

    protected void setSubjects(List<Subject> subjects){
        if(subjects.isEmpty()){
            registerSubjectButton.setDisable(true);
            subjectsComboBox.setDisable(true);
        }else{
            subjectsComboBox.getItems().addAll(subjects);
            subjectsComboBox.getSelectionModel().select(0);
        }
    }

    protected Subject getResult(){
        return subject;
    }

    protected Stage getDialogStage(){
        return dialogStage;
    }

    protected void setDialogStage(Stage diloagStage){
        this.dialogStage = diloagStage;
    }

    public void setRegisteredSubjects(List<Subject> registeredSubjects) {
        this.registeredSubjects = registeredSubjects;
    }
}

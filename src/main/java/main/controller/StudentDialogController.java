package main.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.controller.dialog.Dialogs;
import main.repository.entity.Student;
import sun.security.x509.EDIPartyName;

import static main.utils.Constants.ALL_FIELDS_FILLED;
import static main.utils.Constants.EDIT;
import static main.utils.Constants.ERROR;

public class StudentDialogController {

    @FXML
    private Button createUserButton;

    @FXML
    private TextField usernameTextField;

    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField lastNameTextField;

    @FXML
    private TextField emailTextField;

    private Stage dialogStage;
    private Student student;

    public StudentDialogController(){
        student = new Student();
    }

    public void cancelDialog(ActionEvent actionEvent) {
        student = null;
        dialogStage.close();
    }

    public void createUser(ActionEvent actionEvent) {
        if(allFieldsFilled()){
            student.setUserName(usernameTextField.getText());
            student.setFirstName(firstNameTextField.getText());
            student.setLastName(lastNameTextField.getText());
            student.setEmail(emailTextField.getText());

            dialogStage.close();
        }else{
            Dialogs.showAlertDialog(ERROR, ALL_FIELDS_FILLED);
        }
    }

    private boolean allFieldsFilled(){
        return !(usernameTextField.getText().isEmpty()
                || firstNameTextField.getText().isEmpty()
                || lastNameTextField.getText().isEmpty()
                || emailTextField.getText().isEmpty());
    }

    public void setStudent(Student student){
        this.student = student;
        setStudentTextFields(student);
    }

    private void setStudentTextFields(Student student){
        usernameTextField.setText(student.getUserName());
        firstNameTextField.setText(student.getFirstName());
        lastNameTextField.setText(student.getLastName());
        emailTextField.setText(student.getEmail());

        createUserButton.setText(EDIT);
    }

    public Student getResult(){
        return student;
    }

    protected Stage getDialogStage(){
        return dialogStage;
    }

    protected void setDialogStage(Stage stage){
        dialogStage = stage;
    }
}
